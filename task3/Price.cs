﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal struct Price
    {
        string GoodName;
        public string Shop { get; }
        int UAH;
        int cents;

        public Price(string GoodName, string Shop, int UAH, int cents)
        {
            this.GoodName = GoodName;
            this.Shop = Shop;
            this.UAH = UAH;
            this.cents = cents;
        }

        public void Show()
        {
            Console.WriteLine($"Good name: {GoodName}");
            Console.WriteLine($"Shop name: {Shop}");
            Console.WriteLine($"Price: {UAH},{cents}");
            Console.WriteLine();
        }

        public static void FindByName(string toFind, Price[] arr)
        {
            try
            {
                bool found = false;
                foreach(var price in arr)
                {
                    if(toFind == price.GoodName)
                    {
                        price.Show();
                        found = true;
                    }
                }

                if(!found)
                {
                    throw new Exception("This item was not found.");
                }
            }
            catch(Exception e) 
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
