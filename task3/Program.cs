﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Price[] prices = new Price[2];

                for(int i = 0; i < prices.Length; i++)
                {
                    Console.Write("Input name of good: ");
                    string name = Console.ReadLine();

                    Console.Write("Input shop name: ");
                    string shop = Console.ReadLine();

                    int banknotes;
                    bool validBanknotes = false;
                    Console.Write("Iput price of good (banknotes): ");                 
                    string banknotesInput = Console.ReadLine();
                    validBanknotes = int.TryParse(banknotesInput, out banknotes);

                    int cents;
                    bool validCents = false;
                    Console.Write("Iput price of good (cents): ");
                    string centsInput = Console.ReadLine();
                    validCents = int.TryParse(centsInput, out cents);

                    if(!validBanknotes || !validCents || banknotes <= 0 || cents < 0 || cents > 100)
                    {
                        throw new Exception("Error. Wrong product price.");
                    }

                    Price price = new Price(name, shop, banknotes, cents);
                    prices[i] = price;
                    Console.WriteLine();
                }

                Array.Sort(prices, (w1, w2) => w1.Shop.CompareTo(w2.Shop));
                foreach (var price in prices)
                {
                    price.Show();
                }

                Console.Write("Input name of good you want to find: ");
                string toFind = Console.ReadLine();
                Price.FindByName(toFind, prices);
            }
            catch(Exception e) 
            {
                Console.WriteLine(e.Message);
            }   
        }
    }
}
