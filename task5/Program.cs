﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Write("Input first number: ");
                double a = Convert.ToDouble(Console.ReadLine());
                Console.Write("Input second number: ");
                double b = Convert.ToDouble(Console.ReadLine());

                Console.Write("Choose operatin '+', '-', '*', '/': ");
                string operation = Console.ReadLine();

                if (operation != "+" && operation != "-" && operation != "*" && operation != "/")
                {
                    throw new Exception("Unknown operation.");
                }

                Calculator calc = new Calculator(a, b);
                switch(operation) 
                { 
                    case "+":
                        Console.WriteLine($"{a} + {b} = {calc.Sum()}");
                        break;
                    case "-":
                        Console.WriteLine($"{a} - {b} = {calc.Sub()}");
                        break;
                    case "*":
                        Console.WriteLine($"{a} * {b} = {calc.Mul()}");
                        break;
                    case "/":
                        Console.WriteLine($"{a} / {b} = {calc.Div()}");
                        break;
                }
            }
            catch (Exception ex) 
            {
                Console.WriteLine(ex.Message);
            }

        }
    }
}
