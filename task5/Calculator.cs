﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5
{
    internal class Calculator
    {
        double first_num;
        double second_num;

        public Calculator(double first_num, double second_num)
        {
            this.first_num = first_num;
            this.second_num = second_num;
        }

        public double Sum() => first_num + second_num;
        public double Sub() => first_num - second_num;
        public double Mul() => first_num * second_num;
        public double Div()
        {
            try
            {
                if (second_num == 0)
                {
                    throw new Exception("Division by zero");
                }
                else
                {
                    return first_num / second_num;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return default;
            }
        }
    }
}
