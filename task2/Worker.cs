﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    public struct Worker
    {
        public string name { get; }
        string position;
        int birthYear;
        int expirience;
        
        public Worker(string name, string position, int birthYear, int expirience)
        {
                this.name = name;
                this.position = position;
                this.birthYear = birthYear;
                this.expirience = expirience; 
        }

        public void Show()
        {
            Console.WriteLine($"Name: {name}");
            Console.WriteLine($"Position: {position}");
            Console.WriteLine($"Birth year: {birthYear}");
            Console.WriteLine($"Work expirience: {expirience}");
            Console.WriteLine();
        }
    }
}
