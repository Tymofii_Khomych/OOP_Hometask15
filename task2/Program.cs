﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Worker[] workers = new Worker[3];

                for (int i = 0; i < workers.Length; i++)
                {
                    Console.Write("Input last name and initials: ");
                    string name = Console.ReadLine();

                    Console.Write("Input position: ");
                    string position = Console.ReadLine();

                    int year;
                    bool validYear = false;
                    Console.Write("Input year of birth: ");
                    string yearInput = Console.ReadLine();
                    validYear = int.TryParse(yearInput, out year);

                    if (!validYear)
                    {
                        throw new Exception("Error. Incorrect data format for the year of birth field.");               
                    }

                    if (year < 1920 || year > 2005)
                    {
                        throw new Exception("Error. Invalid date of birth entry.");
                    }

                    Console.Write("Input work experience: ");
                    int expirience = Convert.ToInt32(Console.ReadLine());
                    if (expirience > 2023 - (year + 18))
                    {
                        throw new Exception("Error. Wrong entry of work experience.");
                    }

                    Console.WriteLine();
                    Worker worker = new Worker(name, position, year, expirience);
                    workers[i] = worker;
                }

                Array.Sort(workers, (w1, w2) => w1.name.CompareTo(w2.name));
                foreach (var worker in workers)
                {
                    worker.Show();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
